class ContactMailer < ApplicationMailer

  default from: "Your Mailer <noreply@garciajames.com"
  # default to: "James <jgarcia@garciajames.com>"

  def new_message(contact)
    @contact = contact
    mail(to: "jgarcia@garciajames.com", subject: "Message from #{@contact.name}", from: @contact.email)
  end
end
