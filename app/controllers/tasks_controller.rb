class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:show, :edit, :update, :destroy, :change]

  def index
    @to_do = current_user.tasks.where(state: "to_do")
    @doing = current_user.tasks.where(state: "doing")
    @done = current_user.tasks.where(state: "done")
  end

  def show
  end
  
  def new
    @task = Task.new
  end
  
  def create
    @task = current_user.tasks.new(task_params)

    if @task.save
      flash[:success] = "Task was successfully created"
      redirect_to tasks_path
    else
      flash[:danger] = "Unable to save Task!"
      render 'new'
    end
  end

  def show
  end
  
  def edit
  end

  def update
    if @task.update(task_params)
      flash[:success] = "Task was successfully updated"
      redirect_to tasks_path
    else
      flash[:danger] = "Unable to update Task!"
      render 'edit'
    end
  end

  def destroy
    @task.destroy
    flash[:success] = "Task was destroyed"
    redirect_to tasks_path
  end

  def change
    @task.update_attributes(state: params[:state])
    flash[:success] = "Task State was updated"
    redirect_to tasks_path
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:content, :state)
    end
end