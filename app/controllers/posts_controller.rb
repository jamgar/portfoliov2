class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    if current_user
      @posts = Post.all.order("created_at desc").paginate(page: params[:page], per_page: 10)
    else
      @posts = Post.all.where(publish: true).order("created_at desc").paginate(page: params[:page], per_page: 10)
    end
    @categories = Category.all
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    if @post.save
      flash[:success] = "Article was successfully created"
      redirect_to @post
    else
      flash[:danger] = "Unable to save Article!"
      render 'new'
    end
  end

  def show
    @categories = Category.all
  end

  def edit
  end

  def update
    if @post.update(post_params)
      flash[:success] = "Article was successfully updated!"
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "Article was successfully deleted!"
    redirect_to posts_path
  end

  private

    def post_params
      params.require(:post).permit(:title, :content, :slug, :user_id, :description, :publish, :image_url, category_ids: [])
    end

    def set_post
      @post = Post.friendly.find(params[:id])
    end
end
