class CategoriesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.paginate(page: params[:page], per_page: 5)
    # @categories = Category.all
  end

  def show
    if current_user
      @category_posts = @category.posts.paginate(page: params[:page], per_page: 5)
    else
      @category_posts = @category.posts.where(publish: true).paginate(page: params[:page], per_page: 5)
    end
    @categories = Category.paginate(page: params[:page], per_page: 5)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "Category was created successfully"
      redirect_to categories_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @category.update(category_params)
      flash[:success] = "Category name was update!"
      redirect_to category_path(@category)
    else
      render 'edit'
    end
  end

  def destroy
  end

  private

    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name)
    end
end
