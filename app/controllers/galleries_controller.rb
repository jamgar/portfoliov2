class GalleriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_gallery, only: [:show, :edit, :update, :destory]

  def index
    @galleries = Gallery.all
  end

  def show
  end
    
  def new
    @gallery = Gallery.new
  end
  
  def create
    @gallery = Gallery.new(gallery_params)

    if @gallery.save
      flash[:success] = "Image was added"
      redirect_to @gallery
    else
      flash[:danger] = "Image was not uploaded"
      redirect_to 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @gallery.update(gallery_params)
      flash[:success] = "Image was updated"
      redirect_to @gallery
    else
      flash[:danger] = "Image was not updated"
      redirect_to 'edit'
    end
  end
  
  def destory
    @gallery.destory
    flash[:success] = "Image was deleted"
    redirect_to galleries_path
  end
  
  private
    def set_gallery
      @gallery = Gallery.find(params[:id])
    end
    
    def gallery_params
      params.require(:gallery).permit(:name, :description, :image, :remove_image)
    end
    
end
