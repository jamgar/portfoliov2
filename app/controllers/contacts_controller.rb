class ContactsController < ApplicationController

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.valid?
      ContactMailer.new_message(@contact).deliver
      flash[:success] = "Thank you for contacting me."
      redirect_to root_path
    else
      flash.now[:danger] = "An error occured while delivering this message."
      render :new
    end
  end

  private

    def contact_params
      params.require(:contact).permit(:name, :email, :message)
    end
end
