class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.text :content
      t.integer :user_id
      t.string :state, default: "to_do"
      t.text :content_html
      t.timestamps
    end
  end
end
