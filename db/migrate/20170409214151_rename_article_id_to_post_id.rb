class RenameArticleIdToPostId < ActiveRecord::Migration[5.0]
  def change
    rename_column :post_categories, :article_id, :post_id
  end
end
