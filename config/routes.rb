Rails.application.routes.draw do
  resources :categories #, only: [:create, :show, :edit, :destroy]
  devise_for :users, controllers: { registrations: "registrations" }
  resources :posts
  resources :contacts, only: [:new, :create]
  resources :galleries

  resources :tasks do
    member do
      put :change
    end
  end

  root "pages#home"
end
