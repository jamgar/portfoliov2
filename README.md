# README

This is my new Portfolio. I built using Ruby 5

### Road Map
1. ~~Build a Home page with sections (About, Portfolio, Skills)~~
2. ~~Add Contact Page and to be able to send me messages~~
3. ~~Create Posts and add Categories~~
4. ~~Add a "draft" and "publish" button to the a Post~~
~~     "draft" will not show on to public~~
~~    - "publish" will add it to post index page"~~
5. Add upload images to post
6. Add thumbnails to post index page
7. Add a Portfolio page to list all my projects and add "see more" link to portfolio section
8. Add tags ? Not sure if I need them.

